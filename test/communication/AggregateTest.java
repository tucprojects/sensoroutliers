package communication;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class AggregateTest {

    public AggregateTest() {
    }

    /**
     * Test of an AVG Aggregate.
     */
    @Test
    public void testAvgAggregate() {
//      System.out.println("Aggregate.calculate\n"
//              + "Aggregate.getAggregate\n"
//              + "\tDefaultFunc.FUNCTOR.F_SUM\n"
//              + "\tDefaultFunc.FUNCTOR.F_COUNT");
        
        ArrayList<Double> l = new ArrayList<Double>();
        l.add(10.0);
        l.add(30.0);
        l.add(50.0);
        l.add(70.0);
        l.add(90.0);

        Aggregate<Double> sum = new Aggregate<Double>(0.0);
        for (Double element : l) {
            sum.calculate(element,
                DefaultFunc.getFunctor(DefaultFunc.FUNCTOR.F_SUM));
        }

        Aggregate<Double> count = new Aggregate<Double>(0.0);
        for (Double element : l) {
            count.calculate(element,
                DefaultFunc.getFunctor(DefaultFunc.FUNCTOR.F_COUNT));
        }

        Aggregate<Double> avg =
                new Aggregate<Double>(sum.getAggregate()/count.getAggregate());
        assertEquals(250, sum.getAggregate(), 0);
        assertEquals(5, count.getAggregate(), 0);
        assertEquals(50, avg.getAggregate(), 0);
    }

    /**
     * Test of a F_MIN Aggregate.
     */
    @Test
    public void testMinAggregate() {
//      System.out.println("Aggregate.calculate\n"
//              + "Aggregate.getAggregate\n"
//              + "\tDefaultFunc.FUNCTOR.F_MIN");

        ArrayList<Double> l = new ArrayList<Double>();
        l.add(10.0);
        l.add(30.0);
        l.add(50.0);
        l.add(70.0);
        l.add(90.0);

        Aggregate<Double> min = new Aggregate<Double>(l.get(0));
        for (Double element : l) {
            min.calculate(element,
                DefaultFunc.getFunctor(DefaultFunc.FUNCTOR.F_MIN));
        }

        assertEquals(10, min.getAggregate(), 0);
    }

    /**
     * Test of a F_MIN Aggregate.
     */
    @Test
    public void testMaxAggregate() {
//      System.out.println("Aggregate.calculate\n"
//              + "Aggregate.getAggregate\n"
//              + "\tDefaultFunc.FUNCTOR.F_MAX");

        ArrayList<Double> l = new ArrayList<Double>();
        l.add(10.0);
        l.add(30.0);
        l.add(50.0);
        l.add(70.0);
        l.add(90.0);

        Aggregate<Double> max = new Aggregate<Double>(l.get(0));
        for (Double element : l) {
            max.calculate(element,
                DefaultFunc.getFunctor(DefaultFunc.FUNCTOR.F_MAX));
        }

        assertEquals(90, max.getAggregate(), 0);
    }
}