package communication;

import utilities.Measurement;
import java.util.ArrayList;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class ResponseBuilderTest {

    public ResponseBuilderTest() {
    }

    /**
     * Test of buildWitness method, of class ResponseBuilder.
     */
    @Test
    public void testBuildWitness() {
//        System.out.println("ResponseBuilder.buildWitness");
        ResponseBuilder instance = new ResponseBuilder();
        ArrayList<MsgNodeEntry> w = new ArrayList<MsgNodeEntry>();
        for (int i = 0; i < 10; i++) {
            w.add(new MsgNodeEntry(i, new Measurement(i)));
            instance.buildWitness(i, new Measurement(i));
        }
        Response r = instance.getMessage();
        for (int i = 0; i < 10; i++) {
            assertTrue(r.getWitness().get(i).getNode() == w.get(i).getNode());
            assertTrue(r.getWitness().get(i).getValue().getValue()
                    == w.get(i).getValue().getValue());
        }
    }

    /**
     * Test of buildOutlier method, of class ResponseBuilder.
     */
    @Test
    public void testBuildOutlier() {
//        System.out.println("ResponseBuilder.buildOutlier");
        ResponseBuilder instance = new ResponseBuilder();
        ArrayList<MsgNodeEntry> o = new ArrayList<MsgNodeEntry>();
        for (int i = 0; i < 10; i++) {
            o.add(new MsgNodeEntry(i, new Measurement(i)));
            instance.buildOutlier(i, new Measurement(i));
        }
        Response r = instance.getMessage();
        for (int i = 0; i < 10; i++) {
            assertTrue(r.getOutlier().get(i).getNode() == o.get(i).getNode());
            assertTrue(r.getOutlier().get(i).getValue().getValue()
                    == o.get(i).getValue().getValue());
        }
    }

    /**
     * Test of buildAggregate method, of class ResponseBuilder.
     */
    @Test
    public void testMinBuildAggregate() {
//        System.out.println("ResponseBuilder.buildAggregate (MIN)");
        ResponseBuilder instance = new ResponseBuilder();

        ArrayList<Measurement> valueList = new ArrayList<Measurement>();
        for (int i = 0; i <= 10; i++) {
            valueList.add(new Measurement(i));
        }

        // Building Response (MIN)
        instance.buildAggregate(valueList, AGGR.MIN);
        Response ar = instance.getMessage();

        Map<AGGR, Aggregate<Double>> an = ar.getAggregate();
        Double expected = new Double(0.0);
        
        assertTrue(an.get(AGGR.MIN).getAggregate().compareTo(expected) == 0);
    }

    /**
     * Test of buildAggregate method, of class ResponseBuilder.
     */
    @Test
    public void testMaxBuildAggregate() {
//        System.out.println("ResponseBuilder.buildAggregate (MAX)");
        ResponseBuilder instance = new ResponseBuilder();

        ArrayList<Measurement> valueList = new ArrayList<Measurement>();
        for (int i = 0; i <= 10; i++) {
            valueList.add(new Measurement(i));
        }

        // Building Response (MAX)
        instance.buildAggregate(valueList, AGGR.MAX);
        Response ar = instance.getMessage();

        Map<AGGR, Aggregate<Double>> an = ar.getAggregate();
        Double expected = new Double(10.0);

        assertTrue(an.get(AGGR.MAX).getAggregate().compareTo(expected) == 0);
    }

    /**
     * Test of buildAggregate method, of class ResponseBuilder.
     */
    @Test
    public void testCountBuildAggregate() {
//      System.out.println("ResponseBuilder.buildAggregate (COUNT)");
        ResponseBuilder instance = new ResponseBuilder();

        ArrayList<Measurement> valueList = new ArrayList<Measurement>();
        for (int i = 0; i <= 10; i++) {
            valueList.add(new Measurement(i));
        }

        // Building Response (COUNT)
        instance.buildAggregate(valueList, AGGR.COUNT);
        Response ar = instance.getMessage();

        Map<AGGR, Aggregate<Double>> an = ar.getAggregate();
        Double expected = new Double(11.0);

        assertTrue(an.get(AGGR.COUNT).getAggregate().compareTo(expected) == 0);
    }

    /**
     * Test of buildAggregate method, of class ResponseBuilder.
     */
    @Test
    public void testSumBuildAggregate() {
//      System.out.println("ResponseBuilder.buildAggregate (SUM)");
        ResponseBuilder instance = new ResponseBuilder();

        ArrayList<Measurement> valueList = new ArrayList<Measurement>();
        for (int i = 0; i <= 10; i++) {
            valueList.add(new Measurement(i));
        }

        // Building Response (SUM)
        instance.buildAggregate(valueList, AGGR.SUM);
        Response ar = instance.getMessage();

        Map<AGGR, Aggregate<Double>> an = ar.getAggregate();
        Double expected = new Double(55.0);

        assertTrue(an.get(AGGR.SUM).getAggregate().compareTo(expected) == 0);
    }

    /**
     * Test of buildAggregate method, of class ResponseBuilder. 
     */
    @Test
    public void testAvgBuildAggregate() {
//      System.out.println("ResponseBuilder.buildAggregate (AVG)");
        ResponseBuilder instance = new ResponseBuilder();

        ArrayList<Measurement> valueList = new ArrayList<Measurement>();
        for (int i = 0; i <= 10; i++) {
            valueList.add(new Measurement(i));
        }

        // Building a Response (AVG)
        instance.buildAggregate(valueList, AGGR.AVG);
        Response r = instance.getMessage();

        Map<AGGR, Aggregate<Double>> aggr = r.getAggregate();
        assertTrue(aggr.get(AGGR.SUM).getAggregate().compareTo(new Double(55.0)) == 0);
        assertTrue(aggr.get(AGGR.COUNT).getAggregate().compareTo(new Double(11.0)) == 0);
    }

    /**
     * 2 Response objects are built and checked for object reference equallity.
     * The test is passed when we have 2 different object references. This is
     * a test of the deep copy mechanism of Response class.
     */
    @Test
    public void testBuild2Responses() {
//      System.out.println("ResponseBuilder.buildAggregate "
//                         + "(build 2 MAXresponses)");
        ResponseBuilder instance = new ResponseBuilder();

        for (int i = 0; i < 5; i++) {
            instance.buildWitness(i, new Measurement(i));
        }

        for (int i = 6; i < 10; i++) {
            instance.buildOutlier(i, new Measurement(i*2));
        }

        ArrayList<Measurement> valueList = new ArrayList<Measurement>();
        for (int i = 0; i <= 10; i++) {
            valueList.add(new Measurement(i));
        }

        instance.buildAggregate(valueList, AGGR.MAX);
        Response r1 = instance.getMessage();

        for (int i = 0; i < 5; i++) {
            instance.buildWitness(i, new Measurement(i*2));
        }

        for (int i = 6; i < 10; i++) {
            instance.buildOutlier(i, new Measurement(i*4));
        }

        valueList.clear();
        for (int i = 0; i <= 10; i++) {
            valueList.add(new Measurement(i * 10.0));
        }

        instance.buildAggregate(valueList, AGGR.MAX);
        Response r2 = instance.getMessage();

        // Asserting for object reference equallity
        assertFalse(r1.equals(r2));

        for (int i = 0; i < r1.getWitness().size(); i++) {
            assertFalse(r1.getWitness().get(i).equals(
                    r2.getWitness().get(i)));
        }

        for (int i= 0; i < r1.getOutlier().size(); i++) {
            assertFalse(r1.getOutlier().get(i).equals(
                    r2.getOutlier().get(i)));
        }
        
        assertFalse(r1.getAggregate().get(AGGR.MAX).equals(
                r2.getAggregate().get(AGGR.MAX)));
        
    }
}