package utilities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * import Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class ExtJaccardTest {

    public ExtJaccardTest() {
    }

    /**
     * Test of getCoefficient method, of class Correlation.
     */
    @Test
    public void testEqualGetCoefficient() {
//      System.out.println("Correlation.getCoefficient (equal size arrays");
        double x[] = { 10.01, 10.02, 10.03, 10.04};
        double y[] = { 10, 10.01, 10.02, 10.02 };
        double threshold = 0.80;
        ExtJaccard c = new ExtJaccard();
        assertTrue(threshold <= c.getCoefficient(x, y));
    }

    /**
     * Test of getCoefficient method, of class Correlation. Arrays are not equal
     * sized.
     */
    @Test
    public void testNotEqualGetNCoefficient() {
//      System.out.println("Correlation.getCoefficient (not equal size arrays");
        double x[] = { 10.01, 10.02, 10.03};
        double y[] = { 10, 10.01, 10.02, 10.02 };
        double threshold = 0.80;
        ExtJaccard c = new ExtJaccard();
        assertTrue(threshold > c.getCoefficient(x, y));
    }
}