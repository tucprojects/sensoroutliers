package utilities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class CorrelationTest {

    public CorrelationTest() {
    }

    /**
     * Test of getCoefficient method, of class Correlation.
     */
    @Test
    public void testGetCoefficient() {
//      System.out.println("Correlation.getCoefficient (equal arrays)");
        double x[] = { 10.01, 10.02, 10.03, 10.04 };
        double y[] = { 10, 10.01, 10.02, 10.02 };
        double threshold = 0.80;
        Correlation c = new Correlation();
        assertTrue(threshold <= c.getCoefficient(x, y));
        assertEquals(0.94, c.getCoefficient(x, y), 0.01);
    }

    /**
     * Test of getCoefficient method, of class Correlation when the arrays are
     * not equal sized.
     */
    @Test
    public void testNotEqualSizeGetCoefficient() {
//      System.out.println("Correlation.getCoefficient (not equal arrays)");
        double x[] = { 10.0, 10.0, 10.0, 10.0 };
        double y[] = { 10.0, 10.0, 10.0, 10.0, 10.123 };
        double threshold = 0.80;
        Correlation c = new Correlation();
        assertTrue(threshold > c.getCoefficient(x, y));
        assertEquals(-1.0, c.getCoefficient(x, y), 0);
    }

    /**
     * Test of getCoefficient method, of class Correlation when the arrays have
     * equal elements.
     */
    @Test
    public void testEqualElemGetCoefficient() {
//      System.out.println("Correlation.getCoefficient (equal elements)");
        double x[] = { 10.0, 10.0, 10.0, 10.0 };
        double y[] = { 10.0, 10.0, 10.0, 10.0 };
        double threshold = 0.80;
        Correlation c = new Correlation();
        assertTrue(threshold <= c.getCoefficient(x, y));
        assertEquals(1.0, c.getCoefficient(x, y), 0);
    }
}