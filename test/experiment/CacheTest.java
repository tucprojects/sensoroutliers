package experiment;

import utilities.ExtJaccard;
import utilities.Measurement;
import utilities.Correlation;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class CacheTest {

    public CacheTest() {
    }

    @Test
    public void testNumberOfMeasurements() {
//      System.out.println("Cache.numberOfMeasurements");
        int memSize = 2;
        int cacheSize = 4;
        int testInterval = 4;
        int node1 = 0;
        int node2 = 1;

        Measurement x[] = new Measurement[4];// { 10.01, 10.02, 10.03, 10.04};
        Measurement y[] = new Measurement[4]; //{ 10, 10.01, 10.02, 10.02 };
        for (int i = 0; i < x.length; i++) {
            if (i == x.length - 1)
                x[i] = null;
            else
                x[i] = new Measurement(i);
            y[i] = new Measurement(i);
        }

        Cache instance = new Cache(memSize, cacheSize, testInterval);
        for (int i = 0; i < cacheSize; i++) {
            instance.setMeasurement(node1, i, x[i]);
            instance.setMeasurement(node2, i, y[i]);
        }

        assertTrue(instance.readingsNumber(node1) == x.length-1);
        assertTrue(instance.readingsNumber(node2) == y.length);
    }

    @Test
    public void testCanWitness1() {
//      System.out.println("Cache.canWitness(1-Correlation)");
        int memSize = 2;
        int cacheSize = 4;
        int testInterval = 4;
        int node1 = 0;
        int node2 = 1;

        double x[] = { 10.01, 10.02, 10.03, 10.04};
        double y[] = { 10, 10.01, 10.02, 10.02 };

        Cache instance = new Cache(memSize, cacheSize, testInterval);
        for (int i = 0; i < cacheSize; i++) {
            instance.setMeasurement(node1, i, new Measurement(x[i]));
            instance.setMeasurement(node2, i, new Measurement(y[i]));
        }

        boolean result = instance.canWitness(0, 1, 0, 0.8, new Correlation());
        assertTrue(result);
    }

    @Test
    public void testCanWitness2() {
//      System.out.println("Cache.canWitness(2-Correlation)");
        int memSize = 2;
        int cacheSize = 4;
        int testInterval = 4;
        int node1 = 0;
        int node2 = 1;

        double x[] = { 10.01, 10.02, 10.03, 10.04};
        double y[] = { 10, 10.01, 10.02, 10.02 };

        Cache instance = new Cache(memSize, cacheSize, testInterval);
        for (int i = 0; i < cacheSize; i++) {
            instance.setMeasurement(node1, i, new Measurement(x[i]));
            instance.setMeasurement(node2, i, new Measurement(y[i]));
        }

        boolean result = instance.canWitness(0, 1, 0, 0.8, new Correlation());
        assertTrue(result);
    }

    @Test
    public void testCanWitness3() {
//      System.out.println("Cache.canWitness(3-ExtJaccard)");
        int memSize = 2;
        int cacheSize = 4;
        int testInterval = 4;
        int node1 = 0;
        int node2 = 1;

        double x[] = { 10.0, 10.0, 10.0, 10.0};
        double y[] = { 10.0, 10.0, 10.0, 10.0 };

        Cache instance = new Cache(memSize, cacheSize, testInterval);
        for (int i = 0; i < cacheSize; i++) {
            instance.setMeasurement(node1, i, new Measurement(x[i]));
            instance.setMeasurement(node2, i, new Measurement(y[i]));
        }

        boolean result = instance.canWitness(0, 1, 0, 0.8, new ExtJaccard());
        assertTrue(result);
    }

    @Test
    public void testCanWitness4() {
//      System.out.println("Cache.canWitness(4-ExtJaccard)");
        int memSize = 2;
        int cacheSize = 4;
        int testInterval = 4;
        int node1 = 0;
        int node2 = 1;

        double x[] = { 10.0, 10.0, 10.0, 10.0};
        double y[] = { 10.0, 10.0, 10.0, 10.0 };

        Cache instance = new Cache(memSize, cacheSize, testInterval);
        for (int i = 0; i < cacheSize; i++) {
            instance.setMeasurement(node1, i, new Measurement(x[i]));
            instance.setMeasurement(node2, i, new Measurement(y[i]));
        }

        boolean result = instance.canWitness(0, 1, 0, 0.8, new ExtJaccard());
        assertTrue(result);
    }

    @Test
    public void testCanWitness5() {
//      System.out.println("Cache.canWitness(A lot null measurements)");
        int memSize = 2;
        int cacheSize = 4;
        int testInterval = 4;
        int node1 = 0;
        int node2 = 1;

        double x[] = { 10.01, 10.02, 10.03, 10.04};
        double y[] = { 10, 10.01, 10.02, 10.02 };

        Cache instance = new Cache(memSize, cacheSize, testInterval);
        instance.setMeasurement(node1, 0, null);
        instance.setMeasurement(node2, 0, null);
        
        for (int i = 1; i < cacheSize; i++) {
            if (i % 2 != 0) {
                instance.setMeasurement(node1, i, new Measurement(x[i]));
                instance.setMeasurement(node2, i, new Measurement(y[i]));
            } else {
                instance.setMeasurement(node1, i, null);
                instance.setMeasurement(node2, i, null);
            }
        }

        boolean result = instance.canWitness(0, 1, 0, 0.8, new Correlation());
        assertTrue(!result);
    }
}