package experiment;

import communication.AGGR;
import java.awt.Point;
import org.junit.Test;
import utilities.ExtJaccard;
import static org.junit.Assert.*;

/**
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class InternalNodeTest {

    public InternalNodeTest() {
    }

    /**
     * Making a small sensor tree and test MAX aggregate for 2 epochs. In the
     * second epoch the base-station has an abnormal value;
     */
    @Test
    public void testMaxProcess() {
        double expected;

        InternalNode root = new InternalNode(0, new ExtJaccard());
        root.init(new Point(1,1), 5, 6, 3);
        root.setParent(null);
        InternalNode node1 = new InternalNode(1, new ExtJaccard());
        node1.init(new Point(1,1), 5, 6, 3);
        node1.setParent(root);
        InternalNode node2 = new InternalNode(2, new ExtJaccard());
        node2.init(new Point(1,1), 5, 6, 3);
        node2.setParent(root);
        InternalNode node3 = new InternalNode(3, new ExtJaccard());
        node3.init(new Point(1,1), 5, 6, 3);
        node3.setParent(node2);
        node2.setNodeChild(node3);
        InternalNode node4 = new InternalNode(4, new ExtJaccard());
        node4.init(new Point(1,1), 5, 6, 3);
        node4.setParent(node1);
        node1.setNodeChild(node4);

        root.setNodeChild(node1);
        root.setNodeChild(node2);

        root.newMeasure(10.1);
        node1.newMeasure(10.4);
        node2.newMeasure(10.1);
        node3.newMeasure(10.3);
        node4.newMeasure(10.5);
        node4.process(0, 1, AGGR.MAX);
        node3.process(0, 1, AGGR.MAX);
        node1.process(0, 1, AGGR.MAX);
        node2.process(0, 1, AGGR.MAX);
        root.process(0, 1, AGGR.MAX);

        expected = root.getAggregate().get(AGGR.MAX).getAggregate();
        assertEquals(10.5, expected, 0.01);

        // New epoch having root with an abnormal value.

        root.newMeasure(1000);
        node1.newMeasure(10.42);
        node2.newMeasure(10.3);
        node3.newMeasure(10.3);
        node4.newMeasure(10.41);
        node4.process(1, 1, AGGR.MAX);
        node3.process(1, 1, AGGR.MAX);
        node1.process(1, 1, AGGR.MAX);
        node2.process(1, 1, AGGR.MAX);
        root.process(1, 1, AGGR.MAX);

        expected = root.getAggregate().get(AGGR.MAX).getAggregate();
        assertEquals(10.42, expected, 0.001);
    }

    /**
     * Making a small sensor tree and test AVG aggregate for 2 epochs. In the
     * second epoch the base-station has an abnormal value;
     */
    @Test
    public void testAvgProcess() {
        double expected;
        
        InternalNode root = new InternalNode(0, new ExtJaccard());
        root.init(new Point(1,1), 5, 6, 3);
        root.setParent(null);
        InternalNode node1 = new InternalNode(1, new ExtJaccard());
        node1.init(new Point(1,1), 5, 6, 3);
        node1.setParent(root);
        InternalNode node2 = new InternalNode(2, new ExtJaccard());
        node2.init(new Point(1,1), 5, 6, 3);
        node2.setParent(root);
        InternalNode node3 = new InternalNode(3, new ExtJaccard());
        node3.init(new Point(1,1), 5, 6, 3);
        node3.setParent(node2);
        node2.setNodeChild(node3);
        InternalNode node4 = new InternalNode(4, new ExtJaccard());
        node4.init(new Point(1,1), 5, 6, 3);
        node4.setParent(node1);
        node1.setNodeChild(node4);

        root.setNodeChild(node1);
        root.setNodeChild(node2);

        root.newMeasure(10.1);
        node1.newMeasure(10.12);
        node2.newMeasure(10.11);
        node3.newMeasure(10.09);
        node4.newMeasure(10.08);
        node4.process(0, 1, AGGR.AVG);
        node3.process(0, 1, AGGR.AVG);
        node1.process(0, 1, AGGR.AVG);
        node2.process(0, 1, AGGR.AVG);
        root.process(0, 1, AGGR.AVG);

        expected = root.getAggregate().get(AGGR.SUM).getAggregate();
        expected /= root.getAggregate().get(AGGR.COUNT).getAggregate();
        assertEquals(10.1, expected, 0.01);

        root.newMeasure(10.1);
        node1.newMeasure(100);
        node2.newMeasure(10.1);
        node3.newMeasure(10.3);
        node4.newMeasure(10.5);
        node4.process(1, 1, AGGR.AVG);
        node3.process(1, 1, AGGR.AVG);
        node1.process(1, 1, AGGR.AVG);
        node2.process(1, 1, AGGR.AVG);
        root.process(1, 1, AGGR.AVG);

        expected = root.getAggregate().get(AGGR.SUM).getAggregate();
        expected /= root.getAggregate().get(AGGR.COUNT).getAggregate();
        assertEquals(10.25, expected, 0.001);
    }
}