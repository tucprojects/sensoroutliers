package experiment;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class BatteryTest {

    public BatteryTest() {
    }

    /**
     * Test of transmitEnergy method, of class Battery.
     */
    @Test
    public void testTransmitEnergy() {
//      System.out.println("Battery.transmitEnergy");
        int bits = 8 * 32; // 1 msg
        double distance = 1.0;
        Battery instance = new Battery();
        instance.transmitEnergy(bits, distance);
        double expected = 256 * 50.1 *  Math.pow(10, -9);
        assertEquals(expected, instance.getEnergyConsumption(), 0);
    }

    /**
     * Test of receiveEnergy method, of class Battery.
     */
    @Test
    public void testReceiveEnergy() {
//      System.out.println("Battery.receiveEnergy");
        int bits = 8 * 32; // 1 msg
        Battery instance = new Battery();
        instance.receiveEnergy(bits);
        double expected = 256 * 50 *  Math.pow(10, -9);
        assertEquals(expected, instance.getEnergyConsumption(), 0);
    }

}