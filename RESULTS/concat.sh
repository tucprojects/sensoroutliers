#!/bin/sh

touch results.m
rm results.m
touch results.m

echo "Concatenating result files produced from java program."
echo "clc;\nclear all;\nclose all;\n" > results.m

cat SR_avg_humidity_data_ExtJaccard_2.m SR_avg_humidity_data_ExtJaccard_3.m >> results.m 
cat SR_avg_humidity_data_Correlation_2.m SR_avg_humidity_data_Correlation_3.m >> results.m
cat SR_max_temprature_data_ExtJaccard_2.m SR_max_temprature_data_ExtJaccard_3.m >> results.m
cat SR_max_temprature_data_Correlation_2.m SR_max_temprature_data_Correlation_3.m >> results.m

echo "Removing files produced by java program. Open matlab and run the results.m script."

rm SR_avg_humidity_data_ExtJaccard_2.m SR_avg_humidity_data_ExtJaccard_3.m
rm SR_avg_humidity_data_Correlation_2.m SR_avg_humidity_data_Correlation_3.m
rm SR_max_temprature_data_ExtJaccard_2.m SR_max_temprature_data_ExtJaccard_3.m
rm SR_max_temprature_data_Correlation_2.m SR_max_temprature_data_Correlation_3.m 

