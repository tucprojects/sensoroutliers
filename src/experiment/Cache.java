/*
 * Cache.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package experiment;

import java.util.ArrayList;
import utilities.Coefficient;
import utilities.Measurement;

/**
 * This is the memory_ that every sensor except the leaves has. As described in
 * the paper, its size is determined by the number of epochs we want to save
 * (cache size) and the number of nodes we'll save (memory_ size). Also, a
 * test interval is needed to store new measurements.
 * 
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class Cache {

    private int cacheSize_; // not needed
    private int memSize_; // not needed
    private int testInterval_;
    private Measurement memory_[][];

    public Cache(int memSize, int cacheSize, int testInterval) {
        cacheSize_ = cacheSize;
        testInterval_ = testInterval;
        memSize_ = memSize;
        memory_ = new Measurement[memSize][cacheSize];
        for (int i = 0; i < memSize; i++) {
            for (int j = 0; j < cacheSize; j++) {
                memory_[i][j] = null;
            }
        }
    }

    /**
     * According to the sensor ID and the current epoch, the corresponding
     * measurement object is set. This can be a null value as described in
     * the paper.
     *
     * @param ID    The sensor Id.
     * @param epoch The current epoch.
     * @param value The corresponding value.
     */
    public void setMeasurement(int ID, int epoch, Measurement value) {
        int col = epoch % cacheSize_;
        memory_[ID][col] = value;
    }

    public int readingsNumber(int ID) {
        int numReadings = 0;

        for (Measurement m : memory_[ID]) {
            if (m != null)
                numReadings++;
        }
        return numReadings;
    }

    /**
     * The method, according to 2 given sensor IDs, by going back starting 
     * from (epoch % CacheSize), returns true if testInterval measurements from
     * the same epochs (not null) are similar. False otherwise.
     *
     * @param ID1   The ID of the 1st sensor.
     * @param ID2   The ID of the 2nd sensor.
     * @param epoch The current epoch the system is in.
     * @param threshold The minimum similarity value used for coefficients.
     * @param coef  The Coefficient algorithm that will be used.
     * @return      If similar then true else false.
     */
    public boolean canWitness(int ID1, int ID2, int epoch, double threshold,
            Coefficient coef) {
        // a. get the values of id1 and id2 from the cache
        ArrayList<Double> node1 = new ArrayList<Double>();
        ArrayList<Double> node2 = new ArrayList<Double>();

        int start = epoch % cacheSize_;
        int iter_num = 0;

        if (memory_[ID1][start] == null || memory_[ID2][start] == null)
            return false;
        
        for(int i = start; ; i--) {            
            if (node1.size() == testInterval_ && node2.size() == testInterval_)
                // we got the measures we need
                break;
            
            if (memory_[ID1][i] != null && memory_[ID2][i] != null) {
                // same epoch not null values
                node1.add(memory_[ID1][i].getValue());
                node2.add(memory_[ID2][i].getValue());
            }

            if ( i == 0 &&
                    (node1.size() != testInterval_ &&
                     node2.size() != testInterval_))
                // reached epoch % cacheSize = 0
                i = cacheSize_;

            if (iter_num != 0 && i == start && (node1.size() != testInterval_ ||
                     node2.size() != testInterval_)) {
                // cycled through all measures and didn't much not nulls in
                //the same epoch
                return false;
            }

            iter_num++;
        }

        if (node1.size() != node2.size())
            // Trust no one...
            return false;

        // b. call Coefficient and see their similarity
        double[] valuesID1 = new double[node1.size()];
        double[] valuesID2 = new double[node2.size()];

        for (int i = 0; i < node1.size(); i++) {
            valuesID1[i] = node1.get(i);
            valuesID2[i] = node2.get(i);
        }

        double similarity = coef.getCoefficient(valuesID1, valuesID2);

        // c. return true if their are similar, else false
        return similarity > threshold ? true : false;
    }
}
