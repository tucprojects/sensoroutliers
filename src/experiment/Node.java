/*
 * Node.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package experiment;

import communication.AGGR;
import communication.Aggregate;
import communication.Response;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Map;

/**
 * This is the definition of the methods a sensor node can perform.
 * 
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public interface Node {

    public void newMeasure(double value);
    public void setParent(Node parent);
    public void setNodeChild(Node child);
    public void process(int epoch, int MinSupp, AGGR type);
    public void sendMessage(Response r);
    public Point getPosition();
    public int getNodeID();
    public ArrayList<Node> getChildren();
    public Node getParent();
    public Map<AGGR, Aggregate<Double>> getAggregate();
}
