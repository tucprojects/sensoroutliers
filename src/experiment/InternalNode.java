/*
 * InternalNode.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package experiment;

import communication.AGGR;
import communication.Aggregate;
import communication.MsgNodeEntry;
import communication.Response;
import communication.ResponseBuilder;
import java.awt.Point;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import utilities.Coefficient;
import utilities.Measurement;

/**
 * This is an internal sensor node found between the root and the leaves of the
 * sensor tree. It has a Cache and can run the SensibleAggr-supp algorithm
 * defined by the paper.
 * 
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class InternalNode implements Node {
    
    private int NodeID_;
    private Point position_;
    private Cache memory_;
    private Battery battery_ = new Battery();

    private Node parent_;
    private ArrayList<Node> children_ = new ArrayList<Node>();

    /* The builder to create a response message. */
    private ResponseBuilder builder_ = new ResponseBuilder();

    /* Represents the communication channel of the children to this parent. */
    //private ArrayDeque<Response> inChannel_ = new ArrayDeque<Response>();
    private ArrayList<Response> inChannel_ = new ArrayList<Response>();
   
    private Measurement current_measure_;
    private Map<AGGR, Aggregate<Double>> currentAggr_;
    Coefficient coef_;

    /* This is the threashold that judges the similarity of measurements. */
    static private double threshold_ = 0.8;

    
    /**
     * Constructor.
     * 
     * @param id    The sensor ID.
     * @param coef  The Coefficient function (correlation or extended Jaccard).
     */
    public InternalNode(int id, Coefficient coef) {
        NodeID_ = id;
        coef_ = coef;
        parent_ = null;
    }

    /**
     * Used when setting up the sensor tree, it defines some basic traits of the
     * sensor.
     *
     * @param position      The (x,y) position of this sensor as a Point class.
     * @param parent        The sensor parent of this sensor.
     * @param cacheMemory   For how many sensors it will store measures in the
     *                      Cache.
     * @param cacheSize     How many measures it will store for each sensor.
     * @param testInterval  The number measurements needed for a witnessing
     *                      action.
     */
    public void init(Point position, int cacheMemory, int cacheSize,
            int testInterval) {
        position_ = position;
        // memory_ = new Cache(4096, 10, 6); // Defaults
        memory_ = new Cache(cacheMemory, cacheSize, testInterval);
    }

    /**
     * Sets the new measure of the sensor. This method is called every epoch.
     *
     * @param value The new measurement read from the dataset.
     */
    public void newMeasure(double value) {
        current_measure_ = new Measurement(value);
    }

    /**
     * Set the parent of this sensor.
     * 
     * @param parent    The parent.
     */
    public void setParent(Node parent) {
        parent_ = parent;
    }

    /**
     * Used when setting up the sensor tree, it adds a child to this sensor.
     *
     * @param child The new child.
     */
    public void setNodeChild(Node child) {
        children_.add(child);
    }

    /**
     * This is the core method that gets called when the sensor tree is made and
     * the experiment is set to go. It first sees if there are Response messages
     * in communication channel of the sensor in question. It collects and
     * unifies the witnesses, the outliers and the aggregates received from
     * the sensor's children. The next step is to run the algorithm
     * SensibleAggr-supp described in the paper. A new Response is built with
     * the new information and then is sent to the parent of the sensor.
     * 
     * @param epoch     The current epoch of the experiment.
     * @param MinSupp   The minimum value that 2 sensors must have to be
     *                  considered witnessed.
     * @param type      The type of the aggregate.
     */
    public void process(int epoch, int MinSupp, AGGR type) {
        // This is a leaf sensor node. (LeafNode is deprecated now).
        if (inChannel_.isEmpty()) {
            // a. No need to run the algorithm, this sensor is probably a leaf
            // node since the channel is empty.
            // b. Prepare the message.
            builder_.buildAggregate(null, type);
            builder_.buildOutlier(NodeID_, current_measure_);
            // c. Send the message to the parent with no channel loss.
            Response r = builder_.getMessage();
            parent_.sendMessage(r);
            // d. Energy consumption.
            battery_.transmitEnergy(r.messageSize(),
                                    position_.distance(parent_.getPosition()));
            return;
        }

        int receivedBits = 0;
        Map<Integer, Integer> nodeSupport = new HashMap<Integer, Integer>();
        ArrayList<AGGR> aggrTypes = new ArrayList<AGGR>();
        Map<AGGR, ArrayList<Measurement>> preAlgo = 
                new EnumMap<AGGR, ArrayList<Measurement>>(AGGR.class);
        
        if (type == AGGR.AVG) {
            aggrTypes.add(AGGR.SUM);
            aggrTypes.add(AGGR.COUNT);
            preAlgo.put(AGGR.SUM, new ArrayList<Measurement>());
            preAlgo.put(AGGR.COUNT, new ArrayList<Measurement>());
        } else {
            aggrTypes.add(type);
            preAlgo.put(type, new ArrayList<Measurement>());
        }
        
        // a1. See what you got in the channel from the children.
        ArrayList<MsgNodeEntry> witnesses = new ArrayList<MsgNodeEntry>();
        ArrayList<MsgNodeEntry> outliers = new ArrayList<MsgNodeEntry>();
        ArrayList<Measurement> aggrValues = new ArrayList<Measurement>();

        boolean hasAggr = false;
        int count = 0;
        
        for (Response r : inChannel_) {
            witnesses.addAll(r.getWitness());
            outliers.addAll(r.getOutlier());
            receivedBits += r.messageSize();

            for (AGGR aggrType : aggrTypes) {
                if (r.getAggregate().get(aggrType) != null) {
                    if (aggrType == AGGR.COUNT) {
                        count += r.getAggregate().get(aggrType).getAggregate();
                    }
                    double val = r.getAggregate().get(aggrType).getAggregate();
                    preAlgo.get(aggrType).add(new Measurement(val));
                    hasAggr = true;
                }
            }
        }

        // Construct the new value list. In case of an AVG aggregate, insert
        // AGGR.COUNT zeros in the list.
        if (hasAggr) {
            if(type == AGGR.AVG) {
                int added = 0;
                //aggrValues.addAll(preAlgo.get(AGGR.SUM));
                for (Measurement m : preAlgo.get(AGGR.SUM)) {
                    aggrValues.add(m);
                    added++;
                }
                for (int i = 0; i < count-added; i++)
                    aggrValues.add(new Measurement(0.0));
            } else {
                aggrValues.addAll(preAlgo.get(type));
            }
        }

        // All responses are read.
        inChannel_.clear();
        
        // Parent is an outlier.
        outliers.add(new MsgNodeEntry(NodeID_, current_measure_));

        // a2. For each message in the channel run Battery.receiveEnergy.
        battery_.receiveEnergy(receivedBits);

        // a3. Update the cache and set zero support for outlier nodes.
        for (MsgNodeEntry entry : witnesses) {
            memory_.setMeasurement(entry.getNode(), epoch, entry.getValue());
        }

        for (MsgNodeEntry entry : outliers) {
            memory_.setMeasurement(entry.getNode(), epoch, entry.getValue());
            nodeSupport.put(entry.getNode(), 0);
        }

        // ====================================================================
        // b.  Run the core part of the SensibleAggr-supp algorithm to get the
        // new witnesses, the new outliers and the new measures to be included
        // in the new aggregate.
        
        // Part 1: Search the outlier set.
        for (int j = 0; j < outliers.size(); j++) {
            int nodeIDj = outliers.get(j).getNode();
            // Compare the Sj sensor with the outliers that are after him and
            // increase support of the sensors if they canWitness(ed).
            for (int k = j+1; k < outliers.size(); k++) {
                if (k >= outliers.size())
                    break;

                int nodeIDk = outliers.get(k).getNode();
                if (memory_.canWitness(nodeIDj, nodeIDk, epoch, threshold_,
                        coef_)) {
                    nodeSupport.put(nodeIDj, nodeSupport.get(nodeIDj) + 1);
                    nodeSupport.put(nodeIDk, nodeSupport.get(nodeIDk) + 1);
                }
            }
            // See if the Sj sensor canWitness(ed) by the sensors in the witness
            // list. If yes the increase support of Sj.
            for (int k = 0; k < witnesses.size(); k++) {
                if (memory_.canWitness(nodeIDj, witnesses.get(k).getNode(),
                        epoch, threshold_, coef_)) {
                    nodeSupport.put(nodeIDj, nodeSupport.get(nodeIDj) + 1);
                }
            }
        }
                
        // Part 2: Check sensor support in the outlier list. If it's greater
        // than MinSupp move the sensor to witness list and calculate the
        // aggrregate.
        ArrayList<MsgNodeEntry> toRemove = new ArrayList<MsgNodeEntry>();

        for (MsgNodeEntry node : outliers) {
            if (nodeSupport.get(node.getNode()) >= MinSupp) {
                witnesses.add(node);
                toRemove.add(node);
                aggrValues.add(new Measurement(node.getValue().getValue()));
            }
        }
        outliers.removeAll(toRemove);
        toRemove.clear();

        // Complete the aggregate with the measures of the outliers that were 
        // just been witnessed. Now the Response message has completed the 
        // construction of the aggregate.

        builder_.buildAggregate(aggrValues, type);
        

        // Part 3: Keep in witness list only the sensors with the most
        // readings in the Cache.
        for (int j = 0; j < witnesses.size(); j++) {
            int nodeIDj = witnesses.get(j).getNode();
            for (int k = j+1; k < witnesses.size(); k++) {
                if (k >= witnesses.size())
                    break;
                int nodeIDk = witnesses.get(k).getNode();
                if (memory_.canWitness(nodeIDj, nodeIDk, epoch, threshold_,
                        coef_)) {
                    if (memory_.readingsNumber(nodeIDj) >
                            memory_.readingsNumber(nodeIDk)) {
                        witnesses.remove(k);
                    }
                }
            }
        }
        // ====================================================================
        
        // c.  Prepare the message.
        for (MsgNodeEntry m : witnesses) {
            builder_.buildWitness(m.getNode(), m.getValue());
        }

        for (MsgNodeEntry m : outliers) {
            builder_.buildOutlier(m.getNode(), m.getValue());
        }
        
        Response r = builder_.getMessage();
        
        // Set the current aggregate.
        currentAggr_ = r.getAggregate();

// ******* DEBUGGING PURPOSES ONLY *********************************************
//        System.out.println("===== Sending =====");
//        System.out.println("\tWitnesses");
//        for (MsgNodeEntry m : r.getWitness()) {
//            System.out.println(m.getNode());
//        }
//        System.out.println("\tOutliers");
//        for (MsgNodeEntry m : r.getOutlier()) {
//            System.out.println(m.getNode());
//        }
//        System.out.println("\tAggregate");
//        for (AGGR aggr : aggrTypes) {
//            if (r.getAggregate().get(aggr) != null)
//                System.out.println(r.getAggregate().get(aggr).getAggregate());
//            else
//                System.out.println("null");
//        }
//        System.out.println("===================");
// *****************************************************************************

        // This is not the root of the tree.
        if (parent_ != null) {
            // d. Add the message to the inChannel_ of the parent (transmission).
            parent_.sendMessage(r);
            // e. Energy consumption.
            battery_.transmitEnergy(r.messageSize(),
                    position_.distance(parent_.getPosition()));
        }
    }

    /**
     * Used by a child of this sensor, it adds a Response message to the
     * communication channel.
     *
     * @param r The Response.
     */
    public void sendMessage(Response r) {
        inChannel_.add(r);
    }

    /**
     * Returns the position of this sensor.
     *
     * @return  The position Point.
     */
    public Point getPosition() {
        return position_;
    }

    /**
     * Returns the ID of this sensor.
     *
     * @return  The ID.
     */
    public int getNodeID() {
        return NodeID_;
    }

    /**
     * Returns an array containing all the children of this sensor node.
     *
     * @return  The children!
     */
    public ArrayList<Node> getChildren() {
        return children_;
    }

    /**
     * Returns the aggregate that this sensor has computed for a given epoch.
     *
     * @return  The mapped aggregate!
     */
    public Map<AGGR, Aggregate<Double>> getAggregate() {
        return currentAggr_;
    }

    /**
     * Returns the parent of this sensor. If this is the root then null will be
     * returned.
     * 
     * @return  The parent!
     */
    public Node getParent() {
        return parent_;
    }
}
