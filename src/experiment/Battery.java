/*
 * Battery.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package experiment;

/**
 * The class represents the battery of the sensor and holds the energy
 * consumption. Whenever there is a message transmitted or received there
 * is energy drain. The class only holds what's being done till now and doesn't
 * "kill" the sensor when the battery is out.
 * 
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class Battery {

    private double consumption_;
    private final double Etx_ = 50 * Math.pow(10, -9);  // 50 nJ/bit
    private final double Erx_ = 50 * Math.pow(10, -9);  // 50 nJ/bit
    private final double Erf_ = 100 * Math.pow(10, -12);// 100 pJ/bit/m^2
    
    public Battery() {
        consumption_ = 0;
    }

    /**
     * The method is called everytime the sensor sends a message.
     *
     * @param bits      The size in bits of the message to be sent.
     * @param distance  The distance from this sensor to the receiveing one.
     */
    public void transmitEnergy(int bits, double distance) {
        consumption_ += (Etx_ + Erf_ * Math.pow(distance, 2)) * bits;
    }

    /**
     * The method is called everytime the sensor receives a message.
     *
     * @param bits  The size in bits of the received message.
     */
    public void receiveEnergy(int bits) {
        consumption_ += Erx_ * bits;
    }

    /**
     * This method returns the total energy consumption till now.
     *
     * @return  The energy consumption.
     */
    public double getEnergyConsumption() {
        return consumption_;
    }
}
