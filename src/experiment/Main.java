/*
 * Main.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package experiment;

import communication.AGGR;
import communication.Aggregate;
import java.awt.Point;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import utilities.Coefficient;
import utilities.Correlation;
import utilities.ExtJaccard;

/**
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class Main {

    /**
     * Given a file with the sensor information, the coefficient that will be
     * used and the maximum distance of the sensor's antenna, it constructs the
     * sensor tree.
     * The sensor file begins with a number representing the total number of the
     * sensors in the network and continues with lines delimited with tabs and 
     * ending with \n including the following values:
     * sensor id, isRoot?, computesAgggregate?, x and y coordinates.
     * When the method returns, the client will get an ArrayList containing all
     * the sensor nodes, initialized with all the proper values (parent,
     * children and so on). Starting from the head of the array, the sensor tree
     * is traversed bottom-up, from the leaves to the root.
     *
     * @param sensorFile    The path to the sensor file.
     * @param coef          The coefficient used for measurement similarity.
     * @param maxDist       The maximum distance a node can "hear".
     * @return              The constructed tree.
     * @throws FileNotFoundException
     */
    public static ArrayList<Node> constructTree(String sensorFile,
            Coefficient coef, double maxDist) throws FileNotFoundException {

        // a. Open file containing the position (x,y) of each sensor and map
        // them to the sensor ID.
        Map<Integer, Point> notConnected = new HashMap<Integer, Point>();
        Map<Integer, Point> connected = new HashMap<Integer, Point>();


        ArrayList<Node> tree = new ArrayList<Node>();
        ArrayList<Node> last = new ArrayList<Node>();
        ArrayList<Node> thisLoopLast = new ArrayList<Node>();

        Scanner scanner = new Scanner(new File(sensorFile));

        int column = 0;
        int numNode = scanner.nextInt();
        int nodeID = 0;
        int posX = 0;
        int posY = 0;
        boolean isRoot = false;

        while (scanner.hasNext()) {
            int valueRead = scanner.nextInt();
            column++;
            switch (column) {
                case 1:
                    nodeID = valueRead;
                    break;
                case 2:
                    isRoot = (valueRead == -1 ? true : false);
                case 3:
                    break;
                case 4:
                    posX = valueRead;
                    break;
                case 5:
                    posY = valueRead;
                    if (isRoot) {
                        InternalNode root = new InternalNode(nodeID, coef);
                        root.init(new Point(posX, posY), 47, 10, 6);
                        //tree.add(root);
                        last.add(root);
                        isRoot = false;
                    } else {
                        notConnected.put(nodeID, new Point(posX, posY));
                    }
                    column = 0;
                    break;
            }
        }
        scanner.close();


        while (!notConnected.isEmpty()) {
            for (int k = 0; k < last.size(); k++) {
                InternalNode lastNode = (InternalNode) last.get(k);
                for (int i = 1; i < numNode; i++) {
                    Point current = notConnected.get(i);

                    if (current == null) {
                        continue;
                    }

                    if (current.distance(lastNode.getPosition()) <= maxDist) {
                        InternalNode node = new InternalNode(i, coef);
                        lastNode.setNodeChild(node);
                        node.init(current, 47, 10, 6);
                        node.setParent(lastNode);
                        //tree.add(node);
                        thisLoopLast.add(node);
                        notConnected.remove(i);
                    }
                }
            }
            tree.addAll(last);
            last.clear();
            last.addAll(thisLoopLast);
            thisLoopLast.clear();
        }

        Collections.reverse(tree);
        return tree;
    }

    /**
     * This is an experiment. The measurements of all the sensors for each epoch
     * are given in the file measures and represent temperature. The network
     * will run a MAX aggregate, using the sensor tree that will be produced by
     * the sensors file and the coefficient.
     * When an epoch ends, we get the final aggregate from the root sensor and
     * write the value to a file. The file with the results follows the name
     * pattern max.temp.data.[coefficient type].[minimum supply].
     *
     * @param sensors   The path to the sensor file.
     * @param measures  The path to the measurement file.
     * @param minsupp   The minimum supply used to classify witnesses.
     * @param coef      The coefficient used for measurement similarity.
     */
    public static void maxTempExperiment(String sensors, String measures, 
            int minsupp, Coefficient coef) {

        ArrayList<Node> tree = null;
        Scanner lineScanner = null;
        Scanner measureScanner = null;

        try {
            tree = constructTree(sensors, coef, 10.1);
            lineScanner = new Scanner(new File(measures));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1337);
        }

        lineScanner.useDelimiter(System.getProperty("line.separator"));
        int epoch = 0;

        ArrayList<Double> results = new ArrayList<Double>();
        ArrayList<Integer> epochs = new ArrayList<Integer>();
        
        while (lineScanner.hasNext()) {
            measureScanner = new Scanner(lineScanner.next());
            // New measurements for the nodes. This was implemented 1st with a
            // map but it seems that objects were copied as soon as a value
            // changed.
            for (int i = 0; i < tree.size(); i++) {
                for (int j = 0; j < tree.size(); j++) {
                    if (tree.get(j).getNodeID() == i) {
                        tree.get(j).newMeasure(measureScanner.nextDouble());
                        break;
                    }
                }
            }

            // Process!
            for (Node node : tree) {
                node.process(epoch, minsupp, AGGR.MAX);
            }
            Aggregate<Double> TEST =
                    tree.get(tree.size() - 1).getAggregate().get(AGGR.MAX);

            if (TEST != null) {
                results.add(TEST.getAggregate());
                epochs.add(epoch);
            }
            epoch++;
        }

        lineScanner.close();
        measureScanner.close();
        String prefix = "max_temprature_data_" +
                    coef.getClass().getSimpleName() +
                    "_" + minsupp;
        
        System.out.println(prefix + " got " + results.size() + " aggregates. ");

        String title = "title('Computed MAX Temp., Intel data with noise ("
                + coef.getClass().getSimpleName()
                + ", "
                + minsupp
                + " MinSupp)');";

        writeToFile(title, prefix, results, epochs);
    }

    /**
     * This is an experiment. The measurements of all the sensors for each epoch
     * are given in the file measures and represent humidity. The network
     * will run an AVG aggregate, using the sensor tree that will be produced by
     * the sensors file and the coefficient.
     * When an epoch ends, we get the final aggregate from the root sensor and
     * write the value to a file. The file with the results follows the name
     * pattern avg.humidity.data.[coefficient type].[minimum supply].
     *
     * @param sensors   The path to the sensor file.
     * @param measures  The path to the measurement file.
     * @param minsupp   The minimum supply used to classify witnesses.
     * @param coef      The coefficient used for measurement similarity.
     */
    public static void avgHumExperiment(String sensors, String measures, 
            int minsupp, Coefficient coef) {

        ArrayList<Node> tree = null;
        Scanner lineScanner = null;
        Scanner measureScanner = null;

        try {
            tree = constructTree(sensors, coef, 10.1);
            lineScanner = new Scanner(new File(measures));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1337);
        }

        lineScanner.useDelimiter(System.getProperty("line.separator"));
        int epoch = 0;

        ArrayList<Double> results = new ArrayList<Double>();
        ArrayList<Integer> epochs = new ArrayList<Integer>();
        
        while (lineScanner.hasNext()) {
            measureScanner = new Scanner(lineScanner.next());
            // New measurements for the nodes. This was implemented 1st with a
            // map but it seems that objects were copied as soon as a value
            // changed.
            for (int i = 0; i < tree.size(); i++) {
                for (int j = 0; j < tree.size(); j++) {
                    if (tree.get(j).getNodeID() == i) {
                        tree.get(j).newMeasure(measureScanner.nextDouble());
                        break;
                    }
                }
            }

            // Process!
            for (Node node : tree) {
                node.process(epoch, minsupp, AGGR.AVG);
            }
            Map<AGGR, Aggregate<Double>> TEST = null; //=
                    //tree.get(tree.size() - 1).getAggregate();
            for (Node node : tree) {
                if (node.getNodeID() == 0)
                    TEST = node.getAggregate();
            }
            

            if (!TEST.isEmpty()) {
                double res = TEST.get(AGGR.SUM).getAggregate() /
                        TEST.get(AGGR.COUNT).getAggregate();
                results.add(res);
                epochs.add(epoch);
            }
            epoch++;
        }

        lineScanner.close();
        measureScanner.close();

        String prefix = "avg_humidity_data_" +
                coef.getClass().getSimpleName() +
                "_" + minsupp;
        
        System.out.println(prefix + " got " + results.size() + " aggregates.");
        
        String title = "title('Computed AVG Humidity, Intel data with noise (" 
                + coef.getClass().getSimpleName()
                + ", "
                + minsupp
                + " MinSupp)');";
        
        writeToFile(title, prefix, results, epochs);
    }

    /**
     * Writes the results, transformed to matlab expressions, to a file with .m
     * extension.
     *
     * @param title     The title that will go to the plot.
     * @param prefix    A prefix used for naming.
     * @param results   The array holding the results.
     * @param epochs    An array holding the epochs that we had an aggregate.
     */
    public static void writeToFile(String title, String prefix, 
            ArrayList<Double> results, ArrayList<Integer> epochs) {

        try {
            FileWriter fstream = new FileWriter(
                    "RESULTS" + File.separator + "SR_" + prefix + ".m");
            
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(prefix + " = [ ");
            for (Double d : results) {
                out.write(d.toString() + ", ");
            }
            out.write("];\n");
            out.write("epochs = [ ");
            for (Integer i : epochs) {
                out.write(i.toString() + ", ");
            }
            out.write("];\n");
            out.write("figure;\n"
                    + "plot(epochs, " + prefix + ");\n");
                    //+ "plot(epochs, " + prefix + ");\n");
            out.write("grid on;\n"
                    + "xlabel('Epochs');\n"
                    + "ylabel('Average Humidity');\n"
                    + title
                    + "\n\n");
            out.close();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    /**
     * ! ! ! The entry point ! ! !
     * 
     * @param args
     */
    public static void main(String[] args) {

        String t_giatrakos = "dataset" + File.separator + "t_giatrakos";
        String t_real = "dataset" + File.separator + "t_real";
        String temp = "dataset" + File.separator + "IntelTemperaturePerturbed";
        String humi = "dataset" + File.separator + "IntelHumidityPerturbed";

        for (int minsupp = 2; minsupp <= 3; minsupp++) {
            maxTempExperiment(t_real, temp, minsupp, new Correlation());
            maxTempExperiment(t_real, temp, minsupp, new ExtJaccard());
            avgHumExperiment(t_real, humi, minsupp, new Correlation());
            avgHumExperiment(t_real, humi, minsupp, new ExtJaccard());
        }

        System.out.println("Experiments executed!! Go to directory RESULTS and "
                + "run the script concat.sh");
    }
}

/* Using sensor set t_giatrakos, these are the number of aggregates we get:
 *
 * max_temprature_data_Correlation_2 got 553 aggregates.
 * max_temprature_data_ExtJaccard_2 got 629 aggregates.
 * avg_humidity_data_Correlation_2 got 342 aggregates.
 * avg_humidity_data_ExtJaccard_2 got 484 aggregates.
 * max_temprature_data_Correlation_3 got 445 aggregates.
 * max_temprature_data_ExtJaccard_3 got 620 aggregates.
 * avg_humidity_data_Correlation_3 got 234 aggregates.
 * avg_humidity_data_ExtJaccard_3 got 484 aggregates.
 */

/* Using sensor set t_real, these are the number of aggregates we get (much
 * better from t_giatrakos):
 * 
 * max_temprature_data_Correlation_2 got 628 aggregates.
 * max_temprature_data_ExtJaccard_2 got 629 aggregates.
 * avg_humidity_data_Correlation_2 got 483 aggregates.
 * avg_humidity_data_ExtJaccard_2 got 484 aggregates.
 * max_temprature_data_Correlation_3 got 628 aggregates.
 * max_temprature_data_ExtJaccard_3 got 629 aggregates.
 * avg_humidity_data_Correlation_3 got 483 aggregates.
 * avg_humidity_data_ExtJaccard_3 got 484 aggregates.
 */