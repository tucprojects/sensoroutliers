/*
 * LeafNode.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package experiment;

import communication.AGGR;
import communication.Aggregate;
import communication.Response;
import communication.ResponseBuilder;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Map;
import utilities.Measurement;

/**
 * This is a leaf sensor node. Because it is found at the edges of the sensor
 * tree, according to the paper it doesn't need to have a Cache. Also, it is not
 * efficient to run the algorithm since the only thing to send to its parent is
 * that he is an outlier and nothing more.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
@Deprecated
public class LeafNode implements Node {

    private int NodeID_;
    private Point position_;
    private Battery battery_ = new Battery();
    
    private Node parent_;

    /* The builder to create a response messag. */
    private ResponseBuilder builder_ = new ResponseBuilder();
    
    private Measurement current_measure_;
    
    /**
     * Constructor.
     *
     * @param id    The sensor ID.
     */
    public LeafNode(int id) {
        NodeID_ = id;
    }

     /**
     * Used when setting up the sensor tree, it sets the position of the sensor.
     *
     * @param position      The (x,y) position of this sensor as a Point class.
     */
    public void init(Point position) {
        position_ = position;
    }

    /**
     * Used when setting up the sensor tree, it sets the parent of the sensor.
     * 
     * @param parent        The sensor parent of this sensor.
     */
    public void setParent(Node parent) {
        parent_ = parent;
    }

    /**
     * Sets the new measure of the sensor. This method is called every epoch.
     *
     * @param value The new measurement read from the dataset.
     */
    public void newMeasure(double value) {
        current_measure_ = new Measurement(value);
    }

    /**
     * This is the core method that gets called when the sensor tree is made and
     * the experiment is set to go. This is the leaf sensor version and the
     * actions performed are simple. A leaf sensor does not have any children.
     * So, he doesn't run the SensibleAggr-supp algorithm. He only sends to his
     * father that he is an outlier.
     *
     * @param epoch     The current epoch of the experiment.
     * @param MinSupp   The minimum value that 2 sensors must have to be
     *                  considered witnessed.
     * @param type      The type of the aggregate.
     */
    public void process(int epoch, int MinSupp, AGGR type) {
        // a. no need to run the algorithm, the leaf node is always an outlier
        // b. prepare the message
        builder_.buildOutlier(NodeID_, current_measure_);
        // c. send the message to the parent with no channel loss
        Response r = builder_.getMessage();
        parent_.sendMessage(r);
        // d. energy consumption
        battery_.transmitEnergy(r.messageSize(),
                                position_.distance(parent_.getPosition()));
    }

    /**
     * Used by a child of this sensor, it adds a Response message to the
     * communication channel. If called for this sensor then it will throw an
     * exception. The sensor is a leaf and has no children.
     *
     * @param r The Response.
     */
    public void sendMessage(Response r) {
        throw new UnsupportedOperationException(
                "A leaf sensor node does not accept a Response");
    }

    /**
     * Returns the position of this sensor.
     *
     * @return  The position Point.
     */
    public Point getPosition() {
        return position_;
    }

    /**
     * Returns the ID of this sensor.
     *
     * @return  The ID.
     */
    public int getNodeID() {
        return NodeID_;
    }

    public void setNodeChild(Node child) {
        throw new UnsupportedOperationException(
                "A leaf sensor node does not have any children");
    }

    public ArrayList<Node> getChildren() {
        throw new UnsupportedOperationException(
                "A leaf sensor node does not have any children");
    }

    public Map<AGGR, Aggregate<Double>> getAggregate() {
        throw new UnsupportedOperationException(""
                + "A leaf sensor node does not store an aggregate. "
                + "Ask his parent!");
    }

    public Node getParent() {
        return parent_;
    }
}
