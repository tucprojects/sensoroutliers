/*
 * ExpConstants.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package utilities;

/**
 * Here we define some constants to be used throughout the experiment.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public final class ExpConstants {
    /**
     * The size in bytes of a message header.
     */
    public static final int BYTE_MHEADER_SIZE = 2;

    /**
     * The size in bytes of an aggregate stored in a message.
     */
    public static final int BYTE_AGGR_SIZE = 4;

    /**
     * The size in bytes of the node ID stored in a message.
     */
    public static final int BYTE_NID_SIZE = 2;

    /**
     * The size in bytes of the node measurement stored in a message.
     */
    public static final int BYTE_NVAL_SIZE = 2;
}

