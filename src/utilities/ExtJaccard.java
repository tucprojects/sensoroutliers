/*
 * ExpConstants.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package utilities;

import java.util.Arrays;

/**
 * The extended Jaccard coefficient.
 * 
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class ExtJaccard implements Coefficient {

    /**
     * Using the extended Jaccard coefficient math type, it returns how similar
     * the 2 arrays supplied are.
     *
     * @param x The first values in an array.
     * @param y The second values in an array.
     * @return  The Extended Jaccard Coefficient.
     */
    public double getCoefficient(double[] x, double[] y) {
        if (x.length != y.length)
            return -1;

        if (Arrays.equals(x, y))
            return 1.0;
        
        double dot_product = 0;
        double x_norm = 0;
        double y_norm = 0;

        for (int i = 0; i < x.length; i++) {
            dot_product += x[i] * y[i];
            x_norm += Math.pow(x[i], 2.0);
            y_norm += Math.pow(y[i], 2.0);
        }

        double result = dot_product / (x_norm + y_norm - dot_product);
        return result;
    }

}
