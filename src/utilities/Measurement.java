/*
 * Measurement.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package utilities;

/**
 * This class represents a measurement. We need null values in the sensor
 * cache and that explains the existence of this class. Also, the class is
 * immutable.
 * 
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class Measurement {
    private double mMeasure;

    /**
     * Constructor.
     *
     * @param value The measurement to be stored.
     */
    public Measurement(double value) {
        mMeasure = value;
    }

    /**
     *
     * @return  The measurement.
     */
    public double getValue() {
        return mMeasure;
    }
}
