/*
 * Correlation.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package utilities;

import java.util.Arrays;

/**
 * The class implements the correlation coefficient based on the Pearson method.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class Correlation implements Coefficient {

    /**
     * Calculate the correlation coefficient and return the similarity of the 2
     * method arguments.
     *
     * @param x The first values in an array.
     * @param y The second values in an array.
     * @return  The Correlation coefficient.
     */
    public double getCoefficient(double[] x, double[] y) {
        if (x.length != y.length)
            return -1;

        if (Arrays.equals(x, y)) {
            // This is the case where both x and y are same and we have at the
            // numerator and denumerator zeros.
            return 1.0;
        }

        double result = 0;
        double sum_sq_x = 0;
        double sum_sq_y = 0;
        double sum_coproduct = 0;
        double mean_x = x[0];
        double mean_y = y[0];
        for(int i=2;i<x.length+1;i+=1){
            double sweep = Double.valueOf(i-1)/i;
            double dx = x[i-1]-mean_x;
            double dy = y[i-1]-mean_y;
            sum_sq_x += dx * dx * sweep;
            sum_sq_y += dy * dy * sweep;
            sum_coproduct += dx * dy * sweep;
            mean_x += dx / i;
            mean_y += dy / i;
        }
        double pop_sd_x = (double) Math.sqrt(sum_sq_x/x.length);
        double pop_sd_y = (double) Math.sqrt(sum_sq_y/x.length);
        double cov_x_y = sum_coproduct / x.length;
        result = cov_x_y / (pop_sd_x*pop_sd_y);

        return result;
    }
}
