/*
 * Functor.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package communication;

/**
 * Defines a functor object interface. This is the base for all the aggregate
 * functions like min, max, sum, count, avg and others.
 * 
 * @param <T>   A type that implements Comparable interface like Double and
 *              Integer.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public interface Functor<T extends Comparable> {
    
    public T apply(T obj1, T obj2);
}
