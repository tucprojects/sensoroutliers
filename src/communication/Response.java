/*
 * Response.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package communication;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Map;
import utilities.ExpConstants;

/**
 * This is the response message to a query. It stores the witness list, the
 * outlier list, the computed aggregate until now and the total size of the
 * message.
 * Average aggregate is a special case. It actually consists of 2 aggregates
 * a sum and a count. Also, other more complex aggregates are modeled in the
 * same manner. Simpler aggregates make complex ones. (As a class implementation
 * detail, they are stored in an ArrayList and that's why in some function an
 * index is needed.)
 * 
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class Response implements Message {
    
    private ArrayList<MsgNodeEntry> witnesses_ = new ArrayList<MsgNodeEntry>();
    private ArrayList<MsgNodeEntry> outliers_ = new ArrayList<MsgNodeEntry>();
    private Map<AGGR, Aggregate<Double>> aggr_ =
            new EnumMap<AGGR, Aggregate<Double>>(AGGR.class);
    
    public Response() { }

    /**
     * This is a deep copy constructor the C++ way.
     * 
     * @param r The Response object to copy.
     */
    public Response(Response r) {
        witnesses_ = new ArrayList<MsgNodeEntry>(r.getWitness());
        outliers_ = new ArrayList<MsgNodeEntry>(r.getOutlier());
        aggr_ = new EnumMap<AGGR, Aggregate<Double>>(r.getAggregate());
    }

    /**
     * Returns the total size in bits of the message.
     * @return  Message size in bits.
     */
    public int messageSize() {
        int msgEntrySize =
                8 * (ExpConstants.BYTE_NID_SIZE + ExpConstants.BYTE_NVAL_SIZE);
        int size = ExpConstants.BYTE_MHEADER_SIZE +
                   witnesses_.size() * msgEntrySize +
                   outliers_.size() * msgEntrySize +
                   ExpConstants.BYTE_AGGR_SIZE;
        return size;
    }
    
    /**
     * Adds a witness sensor in the response message.
     *
     * @param witness   It's a pair containing the sensor id and its
     *                  measurement.
     */
    public void addWitness(MsgNodeEntry witness) {
        witnesses_.add(witness);
    }

    /**
     * Returns all the witnesses.
     *
     * @return  An ArrayList with the witnesses.
     */
    public ArrayList<MsgNodeEntry> getWitness() {
        return witnesses_;
    }

    /**
     * Adds an outlier sensor in the response message.
     *
     * @param outlier   It's a pair containing the sensor id and its
     *                  measurement.
     */
    public void addOutlier(MsgNodeEntry outlier) {
        outliers_.add(outlier);
    }

    /**
     * Returns all the outliers.
     * 
     * @return  An ArrayList with the outlier.
     */
    public ArrayList<MsgNodeEntry> getOutlier() {
        return outliers_;
    }

    /**
     * Adds the already computed aggregate of the non-outlier sensors.
     *
     * @param aggr  The aggregate.
     * @param type Defines the aggregate type of the result.
     */
    public void setAggregate(Aggregate<Double> aggr, AGGR type) {
        aggr_.put(type, aggr);
    }

    /**
     * Returns the computed aggregates stored in this response.
     * 
     * @return  All the aggregates.
     */
    public Map<AGGR, Aggregate<Double>> getAggregate() {
        return aggr_;
    }
}
