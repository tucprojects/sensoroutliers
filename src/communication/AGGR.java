/*
 * AGGR.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package communication;

/**
 * Enumenaration AGGR defines the aggregate type to be computed. This is
 * accessible by the client of the user package. <p/>
 * The AVG aggregate type is a special case. In {@link ResponseBuilder} is used
 * only to build a Response message containing an AVG aggregate. <p/>
 * Keep in mind that AVG consists of a COUNT and a SUM and that's how you
 * manipulate the Response message after its construction (by using COUNT and
 * SUM instead of AVG).
 * 
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public enum AGGR {
    /**
     * Represents the MIN aggregate.
     */
    MIN,
    /**
     * Represents the MAX aggregate.
     */
    MAX,
    /**
     * Represents the COUNT aggregate.
     */
    COUNT,
    /**
     * Represents the SUM aggregate.
     */
    SUM,
    /**
     * Represents the AVG aggregate.
     */
    AVG
}
