/*
 * ResponseBuilder.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package communication;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import utilities.Measurement;

/**
 * ResponseBuilder implements the ComBuilder interface. Its purpose is to
 * build a Response object and return it. This class is (and should be) the sole
 * connector between communication and experiment packages. <p/>
 *
 * The preconditions to build a Response are:
 * (i) the witnesses,
 * (ii) the outliers,
 * (iii) an ArrayList with the values of the witnessed sensors for the aggregate,
 * Witnesses and outliers are passed one by one. In contrast, the values need
 * to be passed all together. That is, the witnesses must be computed first to
 * see what values need to be included in the aggregate. <p/>
 *
 * Keep in mind that AVG consists of a COUNT and a SUM and that's how you
 * manipulate the Response message after its construction, by using COUNT and
 * SUM instead of AVG (see {@link AGGR}).
 * 
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class ResponseBuilder implements ComBuilder {
    
    // The product is a Response
    private Response response_;

    public ResponseBuilder() {
        init();
    }

    /**
     * Starts the building of a new Response. This method should be the 1st to
     * call when a Response is finished building, it is returned and a new 1
     * must be built (see method {@link #getMessage()}).
     */
    private void init() {
        response_ = null;
        response_ = new Response();
    }

    /**
     * Adds a single witness sensor in the Response message.
     *
     * @param nodeId    The witnessed sensor id.
     * @param value     The measurement of that sensor.
     */
    public void buildWitness(int nodeId, Measurement value) {
       MsgNodeEntry w = new MsgNodeEntry(nodeId, value);
       response_.addWitness(w);
    }

    /**
     * Adds a single outlier sensor in the Response message.
     *
     * @param nodeId    The outlier sensor id.
     * @param value     The measurement of that sensor.
     */
    public void buildOutlier(int nodeId, Measurement value) {
        MsgNodeEntry w = new MsgNodeEntry(nodeId, value);
        response_.addOutlier(w);
    }

    /**
     * Depending on the aggregate type and the measures of all the witnessed
     * sensors , the aggregate is computed and is added in the Response message.
     *
     * @param valueList An ArrayList with the witnessed sensor measures.
     * @param type      The type of the aggregate.
     */
    public void buildAggregate(ArrayList<Measurement> valueList, AGGR type) {
        ArrayList<Functor<Double>> func = new ArrayList<Functor<Double>>();
        ArrayList<Aggregate<Double>> aggr = new ArrayList<Aggregate<Double>>();
        ArrayList<AGGR> types = new ArrayList<AGGR>();

        if (valueList == null || valueList.isEmpty())
            return;

        switch(type) {
            case MIN:
                func.add(DefaultFunc.getFunctor(DefaultFunc.FUNCTOR.F_MIN));
                aggr.add(new Aggregate<Double>(valueList.get(0).getValue()));
                types.add(AGGR.MIN);
                break;
            case MAX:
                func.add(DefaultFunc.getFunctor(DefaultFunc.FUNCTOR.F_MAX));
                aggr.add(new Aggregate<Double>(valueList.get(0).getValue()));
                types.add(AGGR.MAX);
                break;
            case COUNT:
                func.add(DefaultFunc.getFunctor(DefaultFunc.FUNCTOR.F_COUNT));
                aggr.add(new Aggregate<Double>(0.0));
                types.add(AGGR.COUNT);
                break;
            case SUM:
                func.add(DefaultFunc.getFunctor(DefaultFunc.FUNCTOR.F_SUM));
                aggr.add(new Aggregate<Double>(0.0));
                types.add(AGGR.SUM);
                break;
            case AVG:
                func.add(DefaultFunc.getFunctor(DefaultFunc.FUNCTOR.F_SUM));
                aggr.add(new Aggregate<Double>(0.0));
                types.add(AGGR.SUM);
                func.add(DefaultFunc.getFunctor(DefaultFunc.FUNCTOR.F_COUNT));
                aggr.add(new Aggregate<Double>(0.0));
                types.add(AGGR.COUNT);
                break;
            default:
                // -------------------------------------------------------------
                try {
                    throw new Exception("Wrong AGGR type passed");
                } catch (Exception ex) {
                    Logger.getLogger(ResponseBuilder.class.getName()).
                            log(Level.SEVERE, ex.getMessage(), ex);
                    System.exit(1337);
                }
                // -------------------------------------------------------------
        }
        
        for (Measurement val : valueList) {
            for (int i = 0; i < aggr.size(); i++) {
                aggr.get(i).calculate(val.getValue(), func.get(i));
            }
        }
        
        for (int i = 0; i < aggr.size(); i++) {
            response_.setAggregate(aggr.get(i), types.get(i));
        }
    }

    /**
     * Returns the constructed Response message.
     * 
     * @return  The Response.
     */
    public Response getMessage() {
        Response result = new Response(response_);
        init();
        return result;
    }
}
