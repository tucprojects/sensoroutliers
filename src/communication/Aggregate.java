/*
 * Aggregate.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package communication;

/**
 * The class represents the aggregate that the sensor network needs to compute.
 * It is used for calculating the aggregate in each level of the sensor tree and
 * prepare a Message containing it. Finally, it gets propagated to the next
 * level.
 * 
 * @param <T>   A type that implements Comparable interface like Double and
 *              Integer.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class Aggregate<T extends Comparable> {
    
    private T aggr_;

    /**
     * In the constructor a starting value must be passed. For MIN and MAX a 
     * random value of the measured values must be passed. For SUM
     * and COUNT it must be passed the default value of type T (ex. if double
     * then 0.0).
     * 
     * @param startingValue This is the starting value for the aggregate.
     */
    public Aggregate(T startingValue) {
        aggr_ = startingValue;
    }

    /**
     * According to the given Functor calculate the new aggregate.
     *
     * @param value     The next value (measure).
     * @param functor   The function to be applied.
     */
    public void calculate(T value, Functor<T> functor) {
        aggr_ = functor.apply(aggr_, value);
    }

    /**
     * Returns the computed aggregate.
     *
     * @return  The aggregate.
     */
    public T getAggregate() {
        return aggr_;
    }
}
