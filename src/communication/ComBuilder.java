/*
 * ComBuilder.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package communication;

import java.util.ArrayList;
import utilities.Measurement;

/**
 * The ComBuilder interface represents a builder constructing Messages. It is
 * supposed to be the connector between communication and experiment packages.
 * 
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public interface ComBuilder {
    
    public void buildWitness(int nodeId, Measurement value);
    public void buildOutlier(int nodeId, Measurement value);
    public void buildAggregate(ArrayList<Measurement> value, AGGR type);
    public Message getMessage();
}
