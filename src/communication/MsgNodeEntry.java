/*
 * MsgNodeEntry.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package communication;

import utilities.Measurement;

/**
 * This class represents a pair of a node id and its measurement. It's used
 * to store witnesses and outliers in a response message.
 * 
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class MsgNodeEntry {
    
    private int nodeId_;
    private Measurement value_;
    
    public MsgNodeEntry(int nodeId, Measurement value) {
        nodeId_ = nodeId;
        value_ = value;
    }

    /**
     * Return the Node id.
     * @return  Node id.
     */
    public int getNode() {
        return nodeId_;
    }

    /**
     * Returns the measurement associated with a particular node id.
     * @return  The Measurement of the node.
     */
    public Measurement getValue() {
        return value_;
    }
}
