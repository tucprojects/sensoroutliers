/*
 * DefaultFunc.java
 *
 * Copyright (c) 2011 Vourlakis Nikolas <nvourlakis@gmail.com>.
 *
 * This file is part of sensoroutliers.
 *
 * sensoroutliers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sensoroutliers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sensoroutliers.  If not, see <http ://www.gnu.org/licenses/>.
 */
package communication;

import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;

/**
 * In this class, a default set of functors are being implemented
 * parameterized with Double. There are F_MIN, F_MAX, F_COUNT and F_SUM
 * functions.
 * For AVG, we'll use F_SUM and F_COUNT. Also we are going to need a class
 * pairing tose 2 values.
 * 
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class DefaultFunc {
    
    private static final Map<FUNCTOR, Functor<Double>> DEFAULTS;
    public enum FUNCTOR { F_MIN, F_MAX, F_COUNT, F_SUM }

    static {
        Functor<Double> min= new Functor<Double>() {
            public Double apply(Double obj1, Double obj2) {
                if (obj1.compareTo(obj2) == -1)
                    return obj1;
                return obj2;
            }
        };
        
        Functor<Double> max = new Functor<Double>() {
                public Double apply(Double obj1, Double obj2) {
                    if (obj1.compareTo(obj2) == 1)
                        return obj1;
                    return obj2;
            }
        };

        Functor<Double> count = new Functor<Double>() {
                public Double apply(Double obj1, Double obj2) {
                    return ++obj1;
            }
        };

        Functor<Double> sum = new Functor<Double>() {
                public Double apply(Double obj1, Double obj2) {
                    return obj1+obj2;
                }
        };

        EnumMap<FUNCTOR, Functor<Double>> map =
                new EnumMap<FUNCTOR, Functor<Double>>(FUNCTOR.class);
        
        map.put(FUNCTOR.F_MIN, min);
        map.put(FUNCTOR.F_MAX, max);
        map.put(FUNCTOR.F_COUNT, count);
        map.put(FUNCTOR.F_SUM, sum);
        DEFAULTS = Collections.unmodifiableMap(map);
    }

    public static Functor<Double> getFunctor(FUNCTOR function) {        
        return DEFAULTS.get(function);
    }
}
